# README #

The program can be executed using one of the following commands. ‘1-800-Coding-Challenge.jar’ can be found at the root of the repository. You can find a sample input and dictionary file in ‘Sample Input and Dictionary’ folder.

1) Java –jar 1-800-Coding-Challenge.jar
This command will use a default dictionary file. The default dictionary file has been included in the package.
The command will prompt user to enter phone numbers. Every number much be entered on a new line. After all the numbers have been entered user should enter Ctrl+z.
The output of this command will be displayed on the console.

2) Java –jar 1-800-Coding-Challenge.jar –d <dictionary_file>
This command will allow user to specify a dictionary file. The dictionary must be a ‘.txt’ file or else the program will terminate with an exception. 
The user will be prompted to enter phone numbers, one number per line. Ctrl+z must be entered at the end of the input.
The output will be displayed on the console.

3) Java –jar 1-800-Coding-Challenge.jar <input file> –d <dictionary_file>
This command accepts the input in a file and lets user specify the dictionary with –d option. Please note that both the files much be valid .txt files or else the program will terminate with an exception.
The output will be written to a .txt file. The program will print the output file location on the console.
.
### How do I get set up? ###

Import the project in a workspace. Execute NumberConverter.java with any of the above commands.

* Dependencies

- The application code has been written in JDK 1.8.
- TestNG 6.9.12 has been used for unit testing the code. ‘testng.jar’, ‘jcommander.jar’, ‘bsh-2.0b4.jar’, ‘snakeyaml.jar’, ‘com.google.guava_1.6.0.jar’ have been added to lib folder for this purpose.
- junit-4.10.jar JUnit jar used for unit testing.
- mockito-all-1.9.5.jar and powermock-mockito-release-full-1.5-full.jar are used for mocking dependencies while unit testing.
- javassist-3.20.0-GA.jar  has been used for integrating PowerMock with TestNG.

* How to run tests

Tests can be executed by executing the following command. Please note that testng.xml is located in src/test/resources

java org.testng.TestNG testng.xml

### Design and approach ###

Please refer to 'Design Document - 1-800 Coding Challenge.zip' for the design document
