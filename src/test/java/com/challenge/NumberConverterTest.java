package com.challenge;

import org.testng.annotations.Test;

public class NumberConverterTest {
	
	@Test(expectedExceptions = Exception.class)
	public void main_ShouldThrowAnException_For_InvalidNumberOfArguments() throws Exception {
		String[] args = {"Invalid Argument 0", "Invalid Argument 1", "Invalid Argument 2", "Invalid Argument 3"};
		NumberConverter.main(args);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void main_ShouldThrowAnException_For_TwoInvalidArguments_DictionaryOptionMissing() throws Exception {
		String[] args = {"Invalid Argument 0", "Invalid Argument 1"};
		NumberConverter.main(args);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void main_ShouldThrowAnException_For_ThreeInvalidArguments_DictionaryOptionMissing() throws Exception {
		String[] args = {"Invalid Argument 0", "Invalid Argument 1", "Invalid Argument 2"};
		NumberConverter.main(args);
	}
}
