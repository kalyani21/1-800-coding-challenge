package com.challenge.parser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.testng.Assert.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CommandLineParserWriterTest {

  CommandLineParserWriter commandLineParserWriter = new CommandLineParserWriter();
	
  @Test(dataProvider = "ParseFileOutputMapProvider")
  public void parseFile_ShouldReturn_CorrectOutput(Map<String, List<String>> expectedOutput) {

	  Map<String, List<String>> actualOutput;
	  commandLineParserWriter.setStandardInputStream(getDummyInputStream());
	  try {
		  actualOutput = commandLineParserWriter.parseFile("");
		  assertEquals(actualOutput, expectedOutput);
	} catch (Exception e) {
		fail(e.getMessage());
	}
  }
  
  @Test(expectedExceptions = IllegalArgumentException.class)
  public void parseFile_ShouldThrow_IllegalArgumentException() throws IllegalArgumentException {

	  commandLineParserWriter.parseFile(".//inputFile.txt");
  }
  
  @Test(dataProvider = "outputDataProvider", dataProviderClass = PhoneFileParserWriterTest.class)
  public void writeMapToOutput(Map<String, List<String>> map,
			Map<String, List<String>> formattedToUnformattedNumbersMap,
			String expectedOutputStr) {
	  ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	  commandLineParserWriter.setStandardOutputStream(new PrintStream(outputStream));
	  commandLineParserWriter.writeMapToOutput(map, formattedToUnformattedNumbersMap, null);
	  String actualOutputStr = new String(outputStream.toByteArray());
	  actualOutputStr.trim().equals(expectedOutputStr.trim());
	  assertEquals(actualOutputStr, expectedOutputStr);
  }
  
  private InputStream getDummyInputStream() {
	  String numbers = "76646366722538\n83781247066\n0837812470661\n083781247066\n837812470661";
	  return (new ByteArrayInputStream(numbers.getBytes()));
  }
  
  @DataProvider(name = "ParseFileOutputMapProvider")
  private Object[][] prepareParseFileOutput() {
	  Map<String, List<String>> map = new HashMap<String, List<String>>();
	  List<String> wordsList = new ArrayList<String>();
	  wordsList.add("76646366722538");
	  map.put("76646366722538", wordsList);
	  
	  wordsList = new ArrayList<String>();
	  wordsList.add("0837812470661");
	  map.put("0837812470661", wordsList);
	  
	  wordsList = new ArrayList<String>();
	  wordsList.add("083781247066");
	  map.put("083781247066", wordsList);
	  
	  wordsList = new ArrayList<String>();
	  wordsList.add("837812470661");
	  map.put("837812470661", wordsList);
	  
	  wordsList = new ArrayList<String>();
	  wordsList.add("83781247066");
	  map.put("83781247066", wordsList);
	  
	  return new Object[][]{{map}};
  }
}
