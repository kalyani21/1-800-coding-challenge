package com.challenge.parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.mockito.Mockito.doNothing;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.challenge.exceptions.FileEmptyException;
import com.challenge.helper.Constants;
import com.challenge.helper.Util;

public class PhoneFileParserWriterTest {
	
	private PhoneFileParserWriter parser;
	
	@BeforeClass
	public void init() {
		parser = new PhoneFileParserWriter();
	}
	
	@Test
	public void parseLine_ShouldParseTheGivenNumber() {
		String numberToParse = "5463498";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		parser.parseLine(numberToParse, resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, numbersList) -> {
			assertEquals(numbersList.size(), 1);
			assertEquals(number, numberToParse);
			assertEquals(numbersList.get(0), numberToParse);
		});
	}
	
	@Test
	public void parseLine_ShouldFormatNumber_RemovelWhiteSpacesAndSpecialCharacters() {
		String numberToParse = "  4563 - 498 ";
		String expectedNumber = "4563498";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		parser.parseLine(numberToParse, resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, numbersList) -> {
			assertEquals(numbersList.size(), 1);
			assertEquals(number, expectedNumber);
			assertEquals(numbersList.get(0), numberToParse);
		});
	}
	
	@Test
	public void parseLine_ShouldIgnoreWord_If_ThereAreNoDigitsInTheWord() {
		String numberToParse = "  hgjhy *-* trd ";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		parser.parseLine(numberToParse, resultMap);
		assertEquals(resultMap.size(), 0);
	}
	
	@Test
	public void parseLine_ShouldPrepareNumbersList_If_TheNumberIsRepeated() {
		String formattedNumber = "123890";
		String numberToParse_1 = "123890";
		String numberToParse_2 = "12*38*90";
		String numberToParse_3 = "123 890";
		String numberToParse_4 = "123-890";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		
		parser.parseLine(numberToParse_1, resultMap);
		assertEquals(resultMap.size(), 1);
		
		parser.parseLine(numberToParse_2, resultMap);
		assertEquals(resultMap.size(), 1);
		
		parser.parseLine(numberToParse_3, resultMap);
		assertEquals(resultMap.size(), 1);
		
		parser.parseLine(numberToParse_4, resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, originalNumbersList) -> {
			assertEquals(number, formattedNumber);
			assertEquals(originalNumbersList.size(), 4);
		});
	}
	
	@Test
	public void parseLine_ShouldAddANewMapElement_For_EveryUniqueNumber() {
		String numberToParse_1 = "11111";
		String numberToParse_2 = "2222222";
		String numberToParse_3 = "3333";
		String numberToParse_4 = "44444";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		
		parser.parseLine(numberToParse_1, resultMap);
		assertEquals(resultMap.size(), 1);
		
		parser.parseLine(numberToParse_2, resultMap);
		assertEquals(resultMap.size(), 2);
		
		parser.parseLine(numberToParse_3, resultMap);
		assertEquals(resultMap.size(), 3);
		
		parser.parseLine(numberToParse_4, resultMap);
		assertEquals(resultMap.size(), 4);
	}
	
	@Test(dataProvider = "formattedToOriginalNumbersListMapProvider")
	public void parseFile_ShouldReturnAMap(Map<String, List<String>> resultMap) {
		
		Map<String, List<String>> actualResultMap = new HashMap<String, List<String>>();
		
		try {
			actualResultMap = parser.parseFile(".//src//test//resources//testInputFile.txt");
		} catch (Exception e) {
			fail();
		}
		
		assertEquals(actualResultMap, resultMap);
	}
	
	@Test(expectedExceptions = Exception.class)
	public void parseFile_ShouldThrowAnException_ForInvalidFileFormat() throws Exception {
		// TODO: This is not working. Find another way to mock statics
		doNothing().doThrow(IllegalArgumentException.class).when(Util.class);
		parser.parseFile(".//src//test//resources//testDictionary.xls");
	}
	
	@Test(expectedExceptions = Exception.class)
	public void parseFile_ShouldThrowAnException_ForMissingFile() throws Exception {
		doNothing().doThrow(FileNotFoundException.class).when(Util.class);
		parser.parseFile("..//NoSuchFile.txt");
	}
	
	@Test(expectedExceptions = Exception.class)
	public void parseFile_ShouldThrowAnException_ForEmptyFile() throws Exception {
		doNothing().doThrow(FileEmptyException.class).when(Util.class);
		parser.parseFile(".//src//test//resources//emptyTextFile.txt");
	}
	
	@Test(dataProvider = "formattedToOriginalNumbersListMapProvider")
	public void getMapKeysAsList_ShouldReturn_MapKeySet_AsList(Map<String, List<String>> resultMap) {
		assertTrue(getFormattedToOriginalNumbersListMapForTestFile().length > 0);
		List<String> expectedList = new ArrayList<String>();
		expectedList.addAll(resultMap.keySet());
		List<String> mapKeysAsList = parser.getMapKeysAsList(resultMap);
		assertEquals(mapKeysAsList, expectedList);
	}
	  
	@Test(dataProvider = "outputDataProvider")
	public void writeMapToFile_ShouldSuccessfullyWriteToFile(Map<String, List<String>> map,
										Map<String, List<String>> formattedToUnformattedNumbersMap,
										String expectedFileContentStr) {
		String outputFilePath = ".//src//test//resources//PhoneNumberMapping.txt";
		try {
			parser.writeMapToOutput(map, formattedToUnformattedNumbersMap, outputFilePath);
			
			// Verify file contents
			List<String> fileContents = getFileContentsAsListOfStrings(outputFilePath);
			fileContents.forEach(line -> {
				if( !expectedFileContentStr.contains(line)) {
					fail("Output file contents do not match the expected output.");
				}
			});
			
		} catch (Exception e) {
			fail();
		}
	}
	
	@DataProvider(name = "formattedToOriginalNumbersListMapProvider")
	public static Object[][] getFormattedToOriginalNumbersListMapForTestFile() {
		
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		
		List<String> wordList = new ArrayList<String>();
		wordList.add("12345");
		resultMap.put("12345", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("564-987");
		wordList.add("56-49-87");
		resultMap.put("564987", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add(" 453 68766 ");
		resultMap.put("45368766", wordList);
		
		return new Object[][] {{resultMap}};
	}
	
	@DataProvider(name = "outputDataProvider")
	public static Object[][] createOutputData() {
		Map<String, List<String>> numberToWordMap = new HashMap<String, List<String>>();
		Map<String, List<String>> formattedToUnformattedNumbersMap = new HashMap<String, List<String>>();
		  
		List<String> wordsList = new ArrayList<String>();
		String number = "76646366722538";
		wordsList.add("SONG-NE-NO-RACKET");
		wordsList.add("PONG-NE-NO-RACKET");
		wordsList.add("SONG-ME-NO-RACKET");
		wordsList.add("PONG-ME-NO-RACKET");
		wordsList.add("SONG-NE-ON-RACKET");
		wordsList.add("PONG-NE-ON-RACKET");
		wordsList.add("SONG-ME-ON-RACKET");
		wordsList.add("PONG-ME-ON-RACKET");
		numberToWordMap.put(number, wordsList);
		  
		List<String> originalNumbersList = new ArrayList<String>();
		originalNumbersList.add("7664-63667-22538");
		formattedToUnformattedNumbersMap.put(number, originalNumbersList);
		 
		number = "0837812470661";
		wordsList = new ArrayList<String>();
		wordsList.add("0-TEST-1-AIR-0-NO-1");
		wordsList.add("0-TEST-1-AIR-0-ON-1");
		numberToWordMap.put(number, wordsList);
		  
		originalNumbersList = new ArrayList<String>();
		originalNumbersList.add("083781 2470661");
		formattedToUnformattedNumbersMap.put(number, originalNumbersList);
		  
		number = "083781247066";
		wordsList = new ArrayList<String>();
		wordsList.add("0-TEST-1-AIR-0-NO");
		wordsList.add("0-TEST-1-AIR-0-ON");
		numberToWordMap.put(number, wordsList);

		originalNumbersList = new ArrayList<String>();
		originalNumbersList.add("0837- 8124 - 7066");
		formattedToUnformattedNumbersMap.put(number, originalNumbersList);
		 
		number = "837812470661";
		wordsList = new ArrayList<String>();
		wordsList.add("TEST-1-AIR-0-NO-1");
		wordsList.add("TEST-1-AIR-0-ON-1");
		numberToWordMap.put(number, wordsList);

		originalNumbersList = new ArrayList<String>();
		originalNumbersList.add("837812470661");
		formattedToUnformattedNumbersMap.put(number, originalNumbersList);
		  
		number = "83781247066";
		wordsList = new ArrayList<String>();
		wordsList.add("TEST-1-AIR-0-NO");
		wordsList.add("TEST-1-AIR-0-ON");
		numberToWordMap.put(number, wordsList);
		  
		originalNumbersList = new ArrayList<String>();
		originalNumbersList.add("83781247066");
		formattedToUnformattedNumbersMap.put(number, originalNumbersList);
		 
		String outputStr = getOutputDataAsString();
		return new Object[][] {{numberToWordMap, formattedToUnformattedNumbersMap, outputStr}};
	 }
	
	private static String getOutputDataAsString() {
		String outputString = Constants.OUTPUT_HEADER + Constants.NEW_LINE;
		  outputString += Constants.DOUBLE_QUOTE + "7664-63667-22538" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			Constants.DOUBLE_QUOTE + "76646366722538" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			"[SONG-NE-NO-RACKET, PONG-NE-NO-RACKET, SONG-ME-NO-RACKET, PONG-ME-NO-RACKET, SONG-NE-ON-RACKET, "
				  			+ "PONG-NE-ON-RACKET, SONG-ME-ON-RACKET, PONG-ME-ON-RACKET]" + Constants.NEW_LINE;
		  outputString += Constants.DOUBLE_QUOTE + "083781 2470661" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE +
				  			Constants.DOUBLE_QUOTE + "0837812470661" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			"[0-TEST-1-AIR-0-NO-1, 0-TEST-1-AIR-0-ON-1]" + Constants.NEW_LINE;
		  outputString += Constants.DOUBLE_QUOTE + "0837- 8124 - 7066" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE +
				  			Constants.DOUBLE_QUOTE + "083781247066" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			"[0-TEST-1-AIR-0-NO, 0-TEST-1-AIR-0-ON]" + Constants.NEW_LINE;
		  outputString += Constants.DOUBLE_QUOTE + "837812470661" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			Constants.DOUBLE_QUOTE + "837812470661" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			"[TEST-1-AIR-0-NO-1, TEST-1-AIR-0-ON-1]" + Constants.NEW_LINE;
		  outputString += Constants.DOUBLE_QUOTE + "83781247066" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			Constants.DOUBLE_QUOTE + "83781247066" + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE + 
				  			"[TEST-1-AIR-0-NO, TEST-1-AIR-0-ON]" + Constants.NEW_LINE;
		  
		  return outputString;
	}
	
	private List<String> getFileContentsAsListOfStrings(String filePath) throws IOException {
		
		List<String> retList = new ArrayList<String>();
		try(Stream<String> stream = Files.lines(Paths.get(filePath))) {
			stream.forEach(line -> {
				retList.add(line);
			});
		} catch (IOException e) {
			throw e;
		}
		
		return retList;
	}
}
