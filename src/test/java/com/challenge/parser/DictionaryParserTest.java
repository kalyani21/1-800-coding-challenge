package com.challenge.parser;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import static org.powermock.api.mockito.PowerMockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.challenge.exceptions.FileEmptyException;
import com.challenge.helper.Util;

import static org.testng.Assert.*;

public class DictionaryParserTest {

	private DictionaryParser parser;
	
	@BeforeClass
	public void init() {
		parser = new DictionaryParser();
		mockStatic(Util.class);
	}
	
	@BeforeMethod
	public void initSet() {
		DictionaryParser.uniqueEntriesSet = new TreeSet<String>();
	}
	
	
	@Test
	public void parseLine_ShouldParse_EveryAlphabet() {
		String wordToParse = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String expectedNumber = "22233344455566677778889999";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		parser.parseLine(wordToParse, resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordsList) -> {
			assertEquals(wordsList.size(), 1);
			assertEquals(number, expectedNumber);
			wordsList.forEach(word -> {
				assertEquals(word, wordToParse);
			});
		});
	}
	
	@Test
	public void parseLine_ShouldFormatWord_ToUpperCase() {
		String wordToParse = "photography";
		String expectedWord = "PHOTOGRAPHY";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		parser.parseLine(wordToParse, resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordsList) -> {
			assertEquals(wordsList.size(), 1);
			wordsList.forEach(word -> {
				assertEquals(word, expectedWord);
			});
		});
	}
	
	@Test
	public void parseLine_ShouldFormatWord_RemoveWiteSpacesAndSpecialCharacters() {
		String wordToParse = "  wild - LIFE ##";
		String expectedWord = "WILDLIFE";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		parser.parseLine(wordToParse, resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordsList) -> {
			assertEquals(wordsList.size(), 1);
			wordsList.forEach(word -> {
				assertEquals(word, expectedWord);
			});
		});
	}
	
	@Test
	public void parseLine_ShouldIgnoreWord_If_ThereAreNoAlphabetsInTheWord() {
		String wordToParse = "  #@ - 32 ##";
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		parser.parseLine(wordToParse, resultMap);
		assertEquals(resultMap.size(), 0);
	}
	
	@Test
	public void parseLine_ShouldIgnoreWord_If_ItIsRepeated() {
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		
		parser.parseLine("Call", resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordList) -> {
			assertEquals(wordList.size(), 1);
		});
		
		// Add the same word for the second time
		parser.parseLine("Call", resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordList) -> {
			assertEquals(wordList.size(), 1);
		});
		
		// Add the same word in upper case
		parser.parseLine("CALL", resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordList) -> {
			assertEquals(wordList.size(), 1);
		});
		
		// Add a new word to see if it is parsed
		parser.parseLine("long", resultMap);
		assertEquals(resultMap.size(), 2);
	
	}

	@Test
	public void parseLine_ShouldPrepareList_If_MultipleWordsMapToTheSameNumber() {
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		
		parser.parseLine("Song", resultMap);
		parser.parseLine("PONG", resultMap);
		assertEquals(resultMap.size(), 1);
		
		resultMap.forEach((number, wordList) -> {
			assertEquals(wordList.size(), 2);
		});
	}
	
	@Test
	public void parseLine_ShouldCreateNewMapEntry_For_EveryWordThatMapsToAUniqueNumber() {
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>(); 
		
		parser.parseLine("cricket", resultMap);
		assertEquals(resultMap.size(), 1);
		
		parser.parseLine("WATER", resultMap);
		assertEquals(resultMap.size(), 2);
		
		parser.parseLine("dance", resultMap);
		assertEquals(resultMap.size(), 3);
	}
	
	@Test(dataProvider = "numberToWordListMapProvider")
	public void parseFile_ShouldReturnAMap(Map<String, List<String>> resultMap) {
		
		Map<String, List<String>> actualResultMap = new HashMap<String, List<String>>();
		
		try {
			actualResultMap = parser.parseFile(".//src//test//resources//testDictionary.txt");
		} catch (Exception e) {
			fail();
		}
		
		assertEquals(actualResultMap, resultMap);
	}
	
	@Test(priority = 1, expectedExceptions = Exception.class)
	public void parseFile_ShouldThrowAnException_ForInvalidFileFormat() throws Exception {
		PowerMockito.doThrow(new IllegalArgumentException()).when(Util.class);
		parser.parseFile(".//src//test//resources//testDictionary.xls");
	}
	
	@Test(priority = 1, expectedExceptions = Exception.class)
	public void parseFile_ShouldThrowAnException_ForMissingFile() throws Exception {
		PowerMockito.doThrow(new FileNotFoundException()).when(Util.class);
		parser.parseFile(".//NoSuchFile.txt");
	}
	
	@Test(priority = 1, expectedExceptions = Exception.class)
	public void parseFile_ShouldThrowAnException_ForEmptyFile() throws Exception {
		PowerMockito.doThrow(new FileEmptyException("Empty input file")).when(Util.class);
		parser.parseFile(".//src//test//resources//emptyTextFile.txt");
	}
	
	@Test(dataProvider = "numberToWordListMapProvider")
	public void getMapKeysAsList(Map<String, List<String>> resultMap) {
		List<String> expectedList = new ArrayList<String>();
		expectedList.addAll(resultMap.keySet());
		List<String> mapKeysAsList = parser.getMapKeysAsList(resultMap);
		assertEquals(mapKeysAsList, expectedList);
	}
	
	@DataProvider(name = "numberToWordListMapProvider")
	public static Object[][] getNumberToWordListMapForTestDictionary() {
		
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		
		List<String> wordList = new ArrayList<String>();
		wordList.add("ON");
		wordList.add("NO");
		resultMap.put("66", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("CAT");
		resultMap.put("228", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("HOME");
		resultMap.put("4663", wordList);
		
		return new Object[][] {{ resultMap }};
	}
}
