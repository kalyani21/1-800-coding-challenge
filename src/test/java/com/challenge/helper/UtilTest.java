package com.challenge.helper;

import org.testng.annotations.Test;

import com.challenge.exceptions.FileEmptyException;

import static org.testng.Assert.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UtilTest {
	  
  @Test
  public void isNullOrEmpty_ShouldReturnTrue_For_NullObject() {
    boolean isNullOrEmpty = Util.isNullOrEmpty(null);
    assertEquals(isNullOrEmpty, true);
  }

  @Test
  public void isNullOrEmpty_ShouldReturnFalse_For_NonEmptyString() {
    boolean isNullOrEmpty = Util.isNullOrEmpty("Test String");
    assertEquals(isNullOrEmpty, false);
  }
  
  @Test
  public void isNullOrEmpty_ShouldReturnTrue_For_AnEmptyString() {
    boolean isNullOrEmpty = Util.isNullOrEmpty("");
    assertEquals(isNullOrEmpty, true);
  }
  
  @Test
  public void isNullOrEmpty_ShouldReturnFalse_For_NonEmptyList() {
	List<String> nonEmptyList = new ArrayList<String>();
	nonEmptyList.add("Test String 1");
    boolean isNullOrEmpty = Util.isNullOrEmpty(nonEmptyList);
    assertEquals(isNullOrEmpty, false);
  }
  
  @Test
  public void isNullOrEmpty_ShouldReturnTrue_For_AnEmptyList() {
	List<String> emptyList = new ArrayList<String>();
    boolean isNullOrEmpty = Util.isNullOrEmpty(emptyList);
    assertEquals(isNullOrEmpty, true);
  }
  
  @Test
  public void isNullOrEmpty_ShouldReturnFalse_For_NonEmptyMap() {
	List<String> nonEmptyList = new ArrayList<String>();
	nonEmptyList.add("Test String 1");
	Map<String, List<String>> nonEmptyMap = new HashMap<String, List<String>>();
	nonEmptyMap.put("Test Key 1", nonEmptyList);
    boolean isNullOrEmpty = Util.isNullOrEmpty(nonEmptyMap);
    assertEquals(isNullOrEmpty, false);
  }
  
  @Test
  public void isNullOrEmpty_ShouldReturnTrue_For_AnEmptyMap() {
	Map<String, List<String>> emptyMap = new HashMap<String, List<String>>();
    boolean isNullOrEmpty = Util.isNullOrEmpty(emptyMap);
    assertEquals(isNullOrEmpty, true);
  }


  @Test(expectedExceptions = IllegalArgumentException.class)
  public void verifyFile_ShouldThrow_IllegalArgumentException_For_NonTxtFile() 
		  throws IllegalArgumentException, FileNotFoundException, FileEmptyException {
    Util.verifyFile(".//nonTxtFile.xls");
  }
  
  @Test(expectedExceptions = FileNotFoundException.class)
  public void verifyFile_ShouldThrow_FileNotFoundException_For_MissingFile() 
		  throws IllegalArgumentException, FileNotFoundException, FileEmptyException {
	Util.verifyFile(".//nonExistingFile.txt");
  }

  @Test(expectedExceptions = FileEmptyException.class)
  public void verifyFile_ShouldThrow_FileEmptyException_For_EmptyFile() 
		  throws IllegalArgumentException, FileNotFoundException, FileEmptyException {
	Util.verifyFile(".//src//test//resources//emptyTextFile.txt");
  }

  @Test
  public void verifyFile_ShouldNotThrowAnException_For_NonEmptyTxtFile() {
	try {
		Util.verifyFile(".//src//test//resources//testInputFile.txt");
	} catch (IllegalArgumentException | FileNotFoundException | FileEmptyException e) {
		fail();
	}
  }
  
}
