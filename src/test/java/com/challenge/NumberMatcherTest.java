package com.challenge;

import static org.testng.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mockito.Mockito;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NumberMatcherTest {

	NumberMatcher numberMatcher;
	
	@BeforeClass
	public void setUp() {
		numberMatcher = new NumberMatcher(prepareDictionaryMap());
	}

	@Test
	public void findMatchingWordsForSingleNumber_ShouldReturnOneExactMatch() {
		String inputNumber = "356937";
		String expectedWord = "FLOWER";
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEquals(outputList.size(), 1);
		outputList.forEach(word -> {
			assertEquals(word, expectedWord);
		});
	}

	@Test
	public void findMatchingWordsForSingleNumber_ShouldReturnOneMatch() {
		String inputNumber = "356937826";
		String expectedWord = "FLOWER-VAN";
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEquals(outputList.size(), 1);
		outputList.forEach(word -> {
			assertEquals(word, expectedWord);
		});
	}

	@Test
	public void findMatchingWordsForSingleNumber_ShouldReturnMultipleMatches() {
		String inputNumber = "225563";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("BALL-NE");
		expectedWordList.add("BALL-ME");
		expectedWordList.add("CALL-NE");
		expectedWordList.add("CALL-ME");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEqualsNoOrder(outputList.toArray(), expectedWordList.toArray());
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_FirstDigit_And_ReturnOneMatch() {
		String inputNumber = "1826";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("1-VAN");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEquals(outputList, expectedWordList);
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_FirstDigit_And_ReturnMultipleMatches() {
		String inputNumber = "1228";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("1-CAT");
		expectedWordList.add("1-BAT");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEqualsNoOrder(outputList.toArray(), expectedWordList.toArray());
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_LastDigit_And_ReturnOneMatch() {
		String inputNumber = "3569372";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("FLOWER-2");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEquals(outputList, expectedWordList);
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_LastDigit_And_ReturnMultipleMatches() {
		String inputNumber = "22557";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("CALL-7");
		expectedWordList.add("BALL-7");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEqualsNoOrder(outputList.toArray(), expectedWordList.toArray());
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_MiddleDigit_And_ReturnOneMatch() {
		String inputNumber = "3569370826";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("FLOWER-0-VAN");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEquals(outputList, expectedWordList);
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_MiddleDigit_And_ReturnMultpleMatches() {
		String inputNumber = "2255363";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("CALL-3-ME");
		expectedWordList.add("CALL-3-NE");
		expectedWordList.add("BALL-3-ME");
		expectedWordList.add("BALL-3-NE");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEqualsNoOrder(outputList.toArray(), expectedWordList.toArray());
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_MultipleDigits_And_ReturnOneMatch() {
		String inputNumber = "235693708268";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("2-FLOWER-0-VAN-8");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEquals(outputList, expectedWordList);
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldSkip_MultipleDigits_And_ReturnMultipleMatches() {
		String inputNumber = "122551630";
		List<String> expectedWordList = new ArrayList<String>();
		expectedWordList.add("1-CALL-1-ME-0");
		expectedWordList.add("1-CALL-1-NE-0");
		expectedWordList.add("1-BALL-1-ME-0");
		expectedWordList.add("1-BALL-1-NE-0");
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertNotNull(outputList);
		assertEqualsNoOrder(outputList.toArray(), expectedWordList.toArray());
	}
	
	@Test
	public void findMatchingWordsForSingleNumber_ShouldNotReturnAMatch_As_ConsecutiveDigitsNotMapped() {
		/**
		 * The following number could map to 1-CALL-12-ME-0. 
		 * But since 2 consecutive digits are left unmapped we leave the entire number unmapped.
		 * Hence, no output for this number.
		 */
		String inputNumber = "1225512630";
		List<String> outputList = numberMatcher.findMatchingWordsForSingleNumber(inputNumber);
		assertEquals(outputList.size(), 0);
	}

	@Test(priority = 1)
	public void findMatchingWordsForListOfNumbers() {
		/**
		 * Only verify the interaction here.
		 * Because this calls findMatchingWordsForSingleNumber() for each word in the list.
		 */
		List<String> inputNumbersList = new ArrayList<String>();
		inputNumbersList.add("356937");
		inputNumbersList.add("356937826");
		inputNumbersList.add("225563");
		inputNumbersList.add("1826");
		inputNumbersList.add("1228");
		inputNumbersList.add("3569372");
		inputNumbersList.add("22557");
		inputNumbersList.add("3569370826");
		inputNumbersList.add("2255363");
		inputNumbersList.add("235693708268");
		inputNumbersList.add("122551630");
		inputNumbersList.add("1225512630");
		NumberMatcher numberMatcherMock = Mockito.mock(NumberMatcher.class);
		Mockito.doCallRealMethod().when(numberMatcherMock).findMatchingWordsForListOfNumbers(Mockito.any(List.class));
		numberMatcherMock.findMatchingWordsForListOfNumbers(inputNumbersList);
		Mockito.verify(numberMatcherMock, Mockito.times(inputNumbersList.size())).findMatchingWordsForSingleNumber(Mockito.any(String.class));
		
	}
	
	private Map<String, List<String>> prepareDictionaryMap() {
		Map<String, List<String>> retMap = new HashMap<String, List<String>>();
		List<String> wordList = new ArrayList<String>();
		wordList.add("FLOWER");
		retMap.put("356937", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("NE");
		wordList.add("ME");
		retMap.put("63", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("CALL");
		wordList.add("BALL");
		retMap.put("2255", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("CAT");
		wordList.add("BAT");
		retMap.put("228", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("CAL");
		retMap.put("225", wordList);
		
		wordList = new ArrayList<String>();
		wordList.add("VAN");
		retMap.put("826", wordList);
		
		return retMap;
	}
    
}
