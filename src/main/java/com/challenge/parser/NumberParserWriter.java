package com.challenge.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.challenge.helper.Util;

public abstract class NumberParserWriter implements Parser, OutputWriter {

	public static Set<String> uniqueFormatedNumbersSet = new TreeSet<String>();
	
	@Override
	public Map<String, List<String>> parseLine(String line, Map<String, List<String>> map){
		
		List<String> numbersList;
		// Format the number so that it only contains digits
		String formatedNumber = formatLine(line);
		
		if(!Util.isNullOrEmpty(formatedNumber)) {
			if(addNumber(formatedNumber)) {
				numbersList = new ArrayList<String>();
			} else {
				numbersList = map.get(formatedNumber);
			}
			numbersList.add(line);
			map.put(formatedNumber, numbersList);
		}
		
		return map;
	}
	
	
	private boolean addNumber(String number) {
		return uniqueFormatedNumbersSet.add(number);
	}
	
	private String formatLine(String line) {
		// Remove all characters other than 0-9
		return line.replaceAll("[^0-9]", "");
	}
}
