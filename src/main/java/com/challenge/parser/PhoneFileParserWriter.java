package com.challenge.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.challenge.helper.Constants;
import com.challenge.helper.Util;

public class PhoneFileParserWriter extends NumberParserWriter {

	private static final Logger LOG = Logger.getLogger(PhoneFileParserWriter.class.getName());
	
	@Override
	public void writeMapToOutput(Map<String, List<String>> map,
			Map<String, List<String>> formattedToUnformattedNumbersMap, String filePath) throws Exception {

		File outFile = new File(filePath);
		outFile.getParentFile().mkdirs();
		BufferedWriter bufWriter = new BufferedWriter(new FileWriter(outFile));
		
		try {
			bufWriter.write(Constants.OUTPUT_HEADER);
			
			formattedToUnformattedNumbersMap.forEach((formattedNumber, originalNumbersList) -> {
				originalNumbersList.forEach(originalNumber -> {
					try {
						bufWriter.write(Util.formatOutput(originalNumber, formattedNumber, map.get(formattedNumber)));
						bufWriter.newLine();
					} catch (IOException e) {
						LOG.log(Level.SEVERE, "Exception while writing to the output file", e);
					}
				});
			});
			
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Exception while writing to the output file", e);
		} finally {
			bufWriter.close();
		}
	}

}
