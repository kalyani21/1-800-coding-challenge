package com.challenge.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.challenge.helper.Util;

public interface Parser {

	Map<String, List<String>> parseLine(String line, Map<String, List<String>> map);
	
	default Map<String, List<String>> parseFile(String filePath) throws Exception {
		Logger log = Logger.getLogger(this.getClass().getName());
		
		Map<String, List<String>> retMap = new HashMap<String, List<String>>();
		
		Util.verifyFile(filePath);
		
		try(Stream<String> stream = Files.lines(Paths.get(filePath))) {
			stream.forEach(line -> {
				parseLine(line, retMap);
			});
		} catch (IOException e) {
			log.log(Level.SEVERE, "Error while reading from the file", e);
			throw e;
		}
		
		return retMap;

	}
	
	default List<String> getMapKeysAsList(Map<String, List<String>> map) {
		List<String> keysList = new ArrayList<String>();
		keysList.addAll(map.keySet());
		return keysList;
	};
}
