package com.challenge.parser;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.challenge.helper.Constants;
import com.challenge.helper.Util;

public class CommandLineParserWriter extends NumberParserWriter {
	
	private static final Logger LOG = Logger.getLogger(CommandLineParserWriter.class.getName());
	
	private InputStream standardInputStream;
	private PrintStream standardOutputStream;
	
	@Override
	public Map<String, List<String>> parseFile(String filePath) throws IllegalArgumentException {

		if(!Util.isNullOrEmpty(filePath)) {
			LOG.log(Level.SEVERE, "Input file path must be not be present since we are accepting input from STDIN");
			throw new IllegalArgumentException("Input file path must be not be present since we are accepting input from STDIN");
		}
		
		Map<String, List<String>> retMap = new HashMap<String, List<String>>();
		
		System.out.println(Constants.COMMAND_LINE_MESSAGE);

		InputStream instream = System.in;

		System.setIn((getStandardInputStream() == null ? System.in : getStandardInputStream()));
		Scanner stdin = new Scanner(System.in);
		try {
			while(stdin.hasNextLine()) {
				String line = stdin.nextLine();
				parseLine(line, retMap);
			}
		} finally {
			System.setIn(instream);
			stdin.close();
		}
		
		return retMap;
	}
	
	@Override
	public void writeMapToOutput(Map<String, List<String>> map,
			Map<String, List<String>> formattedToUnformattedNumbersMap, String filePath) {
		
		PrintStream outStreamToWrite = (getStandardOutputStream() == null) ? System.out : getStandardOutputStream();
		outStreamToWrite.println(Constants.OUTPUT_HEADER);
		formattedToUnformattedNumbersMap.forEach((formattedNumber, originalNumbersList) -> {
			
			originalNumbersList.forEach(originalNumber -> {
				
				outStreamToWrite.println(Util.formatOutput(originalNumber, formattedNumber, map.get(formattedNumber)));
			});
		});
		
		if(getStandardOutputStream() != null) {
			outStreamToWrite.close();
		}
	}

	public InputStream getStandardInputStream() {
		return this.standardInputStream;
	}

	public void setStandardInputStream(InputStream standardInputStream) {
		this.standardInputStream = standardInputStream;
	}

	public PrintStream getStandardOutputStream() {
		return standardOutputStream;
	}

	public void setStandardOutputStream(PrintStream standardOutputStream) {
		this.standardOutputStream = standardOutputStream;
	}
}
