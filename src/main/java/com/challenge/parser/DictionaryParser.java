package com.challenge.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.challenge.helper.Constants;
import com.challenge.helper.Util;

public class DictionaryParser implements Parser {

	public static Set<String> uniqueEntriesSet = new TreeSet<String>();
	
	@Override
	public Map<String, List<String>> parseLine(String line, 
			Map<String, List<String>> wordNumberMap) {
		
		String numberStr = Constants.EMPTY_STR;
		String word = Constants.EMPTY_STR;
			
		// Remove special characters and convert to upper case.
		line = formatDictionaryEntry(line);

		/**
		 * Check if the line is duplicate entry.
		 * Skip over to the next line if this is not a valid line.
		 */
		if(validateDictionaryEntry(line)) {
								
			// Map the line to a number using the number encoding map.
			for(int i = 0; i < line.length(); i++) {
				String digitStr = Constants.numberEncoding.get(Constants.EMPTY_STR + line.charAt(i));
					
				word += line.charAt(i);
				numberStr += digitStr;
				
			}
			
			/**
			 * Now we have numberStr which is the number mapping for this dictionary entry
			 * Create a new map entry with numberStr as the key and a List of matching words as the value
			 */
			List<String> wordsList = wordNumberMap.get(numberStr);
			
			if(wordsList == null) {
				// Insert the first matching word for a number
				wordsList = new ArrayList<String>();
				wordsList.add(word);
				wordNumberMap.put(numberStr, wordsList);
			} else {
				// Add the word to the existing list of words.
				wordsList.add(word);
				wordNumberMap.put(numberStr, wordsList);
			}
		}
	
		return wordNumberMap;
	}
	
	private static boolean validateDictionaryEntry(String line) {

		if(Util.isNullOrEmpty(line) || uniqueEntriesSet.contains(line))
			return false;
		else {
			uniqueEntriesSet.add(line);
			return true;
		}
	}
	
	/**
	 * 
	 * @param line - A line from the dictionary
	 * @return - A string after removing white spaces and special characters from line. 
	 */
	private static String formatDictionaryEntry(String line) {
		// Remove all characters other than a-z and A-Z.
		line = line.replaceAll("[^a-zA-Z]", "").toUpperCase();
		return line;
	}
	
}
