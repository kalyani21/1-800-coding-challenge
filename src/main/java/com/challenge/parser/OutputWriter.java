package com.challenge.parser;

import java.util.List;
import java.util.Map;

public interface OutputWriter {

	void writeMapToOutput(Map<String, List<String>> map, 
							Map<String, List<String>> formattedToUnformattedNumbersMap,
							String filePath) throws Exception;
}
