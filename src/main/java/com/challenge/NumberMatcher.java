package com.challenge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.challenge.helper.Constants;
import com.challenge.helper.Util;

public class NumberMatcher {
    private static final Logger LOGGER = Logger.getLogger(NumberMatcher.class.getName());
    private Map<String, List<String>> wordNumberMap;

    public NumberMatcher(Map<String, List<String>> wordNumberMap) {
        this.wordNumberMap = wordNumberMap;
    }
    
    /**
     * Finds possible word matches for numbers in the 'numbersList'
     */
    public Map<String, List<String>> findMatchingWordsForListOfNumbers(List<String> numbersList) {
        Map<String, List<String>> numberToWordsMap = new HashMap<String, List<String>>();

        numbersList.forEach(
            number -> {
                List<String> matchingWordsList = findMatchingWordsForSingleNumber(number);

                if (!Util.isNullOrEmpty(matchingWordsList)) {
                    numberToWordsMap.put(number, matchingWordsList);
                }
            });

        return numberToWordsMap;
    }

    /**
     * Finds possible word matches for the given number.
     */
    public List<String> findMatchingWordsForSingleNumber(String number) {
        LOGGER.log(Level.FINEST, "Finding matches for number " + number);

        List<String> matches = new ArrayList<String>();

        // Validate and format the number before starting the search. Return empty list if not a valid number.
        String formattedNumber = formatNumber(number);

        if (!formattedNumber.equals(Constants.EMPTY_STR)) {
            List<String> masterList = new ArrayList<String>(wordNumberMap.keySet());

            // Prepare matrix to store intermediate result.
            int[][] matrix = prepareMatrix(formattedNumber, masterList);

            // From the matrix obtained above find substrings that can be concatenated to form the exact match for the formattedNumber
            List<String> matchingNumbersList = processMatrix(matrix, formattedNumber);

            // If no exact match is found, then we will try skipping digits in between to find the matches.
            if (matchingNumbersList.isEmpty()) {
                LOGGER.log(Level.FINE,
                           "Could not find any exact match for " + formattedNumber
                           + ". Hence, finding matches by skipping digits now");
                matchingNumbersList = processMatrixAfterNoExactMatchFound(matrix, formattedNumber);
            }

            matches = prepareMatchingWordsList(matchingNumbersList);

            if (Util.isNullOrEmpty(matches)) {
                LOGGER.log(Level.FINE, "Could not find any match for " + formattedNumber);
            }
        }

        return matches;
    }

    /**
     * Formats the given number by removing characters other than digits.
     */
    private String formatNumber(String number) {

        // Remove all characters other than 0-9.
        String returnStr = number.replaceAll("[^\\d]", "");

        return returnStr;
    }

    /**
     * This method returns the possible matches for a given phone number.
     * Matches are prepared based on matching number substrings found from previous steps.
     */
    private List<String> prepareMatchingWordsList(List<String> matchingNumbersList) {
        List<String> matches = new ArrayList<String>();

        if (!matchingNumbersList.isEmpty()) {
            matchingNumbersList.forEach(
                matchingNumber -> {
                    List<String>    wordMatches     = new ArrayList<String>();
                    StringTokenizer stringTokenizer = new StringTokenizer(matchingNumber, Constants.DASH);

                    while (stringTokenizer.hasMoreTokens()) {
                        String       matchingNumberPart = stringTokenizer.nextToken();
                        List<String> matchingWordParts  = wordNumberMap.get(matchingNumberPart);

                        /**
                         * Words list is empty.
                         * This means that a digit from the number could not be mapped. 
                         * Hence, it has been skipped.
                         * So, treat this number as if it were a matching word and proceed.
                         */
                        if (matchingWordParts == null) {
                            matchingWordParts = new ArrayList<String>();
                            matchingWordParts.add(matchingNumberPart);
                        }

                        if (wordMatches.isEmpty()) {
                            matchingWordParts.forEach(
                                part -> {
                                    wordMatches.add(part);
                                });
                        } else {
                            List<String> updatedWordMatches = new ArrayList<String>();

                            matchingWordParts.forEach(
                                part -> {
                                    wordMatches.forEach(
                                        matchingWord -> {
                                            updatedWordMatches.add(matchingWord + Constants.DASH + part);
                                        });
                                });
                            wordMatches.removeAll(wordMatches);
                            wordMatches.addAll(updatedWordMatches);
                        }
                    }

                    matches.addAll(wordMatches);
                });
        }

        return matches;
    }

    /**
     *
     * @return - Returns a 2D array where number_of_rows = number_of_columns = number.length().
     *
     * All substrings(numbers, not words) from the dictionary are stored in the 2D array.
     * Each substring is placed at index [rowId][columnId] 
     * where rowId = number.indexOf(substring) & columnId = substring.length() -1.
     * Hence, rowId is the starting index of the substring in the number.
     * and columnId is the ending index of the substring in the number.
     */
    private int[][] prepareMatrix(String number, List<String> masterList) {
        int[][] matrix = new int[number.length()][number.length()];

        /**
         * Continue only if the length of the number in the dictionary is <= the number being searched.
         * Then check if the number in the dictionary is a substring of the number being searched.
         * Add such numbers from the dictionary into an intermediate array(matrix).
         */
        masterList.forEach(
            masterNumber -> {
                if ((masterNumber.length() <= number.length()) && number.contains(masterNumber)) {
                    String tempNumber = number;
                    String tempMaster = masterNumber.replaceFirst(Constants.EMPTY_STR + masterNumber.charAt(0), "*");

                    // In order to trace all occurrences of the substring use the while loop
                    while (tempNumber.indexOf(masterNumber) >= 0) {
                        matrix[tempNumber.indexOf(masterNumber)][masterNumber.length() - 1] = 1;
                        tempNumber                                                          =
                            tempNumber.replaceFirst(masterNumber,
                                                    tempMaster);
                    }
                }
            });

        return matrix;
    }

    /**
     * @return - Returns a list of matching words
     */
    private List<String> processMatrix(int[][] matrix, String number) {
        boolean      matchesFound = false;
        List<String> matches      = new ArrayList<String>();

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < number.length(); j++) {
                String match  = Constants.EMPTY_STR;
                String parts  = Constants.EMPTY_STR;
                int    row    = i,
                       column = j;

                while (!match.equals(number)) {
                    int flag = traverseMatrix(row, column, number, matrix, false);

                    if (flag == 1) {
                        String partialMatch = number.substring(row, row + column + 1);

                        match += partialMatch;
                        parts = addPartialMatchToParts(parts, partialMatch);
                        row   = row + column + 1;

                        // Following if will ensure that we don't lose the track of j
                        if (j == 0) {
                            j = column;
                        }

                        // Change column back to 0.
                        column = 0;
                    } else if (flag == 2) {
                        column++;
                    } else {
                        break;
                    }
                }

                if (match.equals(number)) {
                    matchesFound = true;
                    matches.add(parts);
                }
            }

            if (matchesFound) {
                break;
            }
        }

        return matches;
    }

    /**
     * @return - Returns a list of matching words.
     *      This is prepared by traversing the matrix 
     *      and concatenating the substrings to form the possible combinations for matches.
     */
    private List<String> processMatrixAfterNoExactMatchFound(int[][] matrix, String number) {
        Boolean      matchesFound          = false;
        Boolean      previousDigitSkipped  = false;
        Boolean      firstDigitSkipped     = false;
        Boolean      noMoreMatchesExpected = false;
        List<String> matches               = new ArrayList<String>();
        int          lastColumnIndex       = 0;

        for (int rowIndex = 0; rowIndex < 2; rowIndex++) {
            for (int columnIndex = lastColumnIndex; columnIndex < number.length(); columnIndex++) {
                String match = ((rowIndex == 1) && firstDigitSkipped)
                               ? number.substring(0, 1)
                               : Constants.EMPTY_STR;
                String parts = ((rowIndex == 1) && firstDigitSkipped)
                               ? number.substring(0, 1)
                               : Constants.EMPTY_STR;

                firstDigitSkipped = firstDigitSkipped
                                    ? false
                                    : firstDigitSkipped;

                int row    = rowIndex,
                    column = columnIndex;

                while (!match.equals(number)) {
                    String partialMatch = "";
                    int    flag         = traverseMatrix(row, column, number, matrix, previousDigitSkipped);

                    switch (flag) {
                    case 1 :
                        previousDigitSkipped = previousDigitSkipped
                                               ? false
                                               : previousDigitSkipped;
                        partialMatch         = number.substring(row, row + column + 1);
                        match                += partialMatch;
                        parts                = addPartialMatchToParts(parts, partialMatch);
                        columnIndex          = (columnIndex == 0)
                                               ? column
                                               : columnIndex;
                        row                  = row + column + 1;
                        column               = 0;

                        break;

                    case 2 :
                        column++;

                        break;

                    case 3 :

                        /**
                         * Since no match is found, we will have to skip this digit.
                         * The flag 'previousDigitSkipped' has to be made TRUE now so that we do not skip 2 digits in a row.
                         * Increment row by 1 to continue the search from the next digit.
                         */
                        previousDigitSkipped = true;
                        partialMatch         = number.substring(row, row + 1);
                        match                += partialMatch;
                        parts                = addPartialMatchToParts(parts, partialMatch);
                        firstDigitSkipped    = (row == 0)
                                               ? true
                                               : firstDigitSkipped;

                        /**
                         * This is a special case where a non matching number appears at index 0.
                         * Hence, we need to increment rowIndex in order to proceed to the next row
                         */
                        rowIndex = (row == 0)
                                   ? (rowIndex + 1)
                                   : rowIndex;
                        row++;
                        column = 0;

                        break;

                    case 4 :
                        previousDigitSkipped = false;
                        lastColumnIndex      = columnIndex;

                        /**
                         * This means we have already skipped digits at positions 0 and 1.
                         * Hence, there is no chance of finding a match for this number.
                         * So, just break out of all the loops and return to the caller.
                         */
                        noMoreMatchesExpected = (row == 1)
                                                ? true
                                                : noMoreMatchesExpected;

                        break;
                    }

                    if (flag == 4) {
                        break;
                    }
                }

                // Break since there is no chance of finding a match because Index 0 and 1 could not be matched.
                if (noMoreMatchesExpected) {
                    break;
                }

                if (match.equals(number)) {
                    matchesFound = true;
                    matches.add(parts);
                } else {
                    columnIndex = lastColumnIndex;
                }
            }

            // Break since there is no chance of finding a match because Index 0 and 1 could not be matched.
            if (noMoreMatchesExpected) {
                break;
            }

            /**
             * All matches starting at index 0 have been found.
             * Hence, there is no need to loop further.
             */
            if (matchesFound) {
                break;
            }
        }

        return matches;
    }

    private int traverseMatrix(int row, int column, String number, int[][] matrix, boolean previousDigitSkipped) {
        int returnValue = 0;

        if ((column < number.length()) && (row < number.length())) {
            if (matrix[row][column] == 1) {
                returnValue = 1;
            } else {
                returnValue = 2;
            }
        } else {
            if (!previousDigitSkipped) {
                returnValue = 3;
            } else {
                returnValue = 4;
            }
        }

        return returnValue;
    }

    private String addPartialMatchToParts(String parts, String partialMatch) {
        String returnStr = parts;

        if (returnStr.length() > 0) {
            returnStr += Constants.DASH + partialMatch;
        } else {
            returnStr = partialMatch;
        }

        return returnStr;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
