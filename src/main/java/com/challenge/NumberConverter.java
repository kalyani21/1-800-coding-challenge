package com.challenge;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.challenge.exceptions.InvalidCommandException;
import com.challenge.helper.Constants;
import com.challenge.parser.CommandLineParserWriter;
import com.challenge.parser.DictionaryParser;
import com.challenge.parser.NumberParserWriter;
import com.challenge.parser.Parser;
import com.challenge.parser.PhoneFileParserWriter;

public class NumberConverter {
	
	private static Parser dictionaryParser;
	private static NumberParserWriter phoneNumberParser;
	private static String dictionaryPath;
	private static String inputFilePath;
	
	public static void main(String[] args) throws Exception {
		
		// Configure LogManager here and start logging
		congigureLogManager();
		
		Logger logger = Logger.getLogger(NumberConverter.class.getName());
		
		try {
			
			/** 
			 * 1: Initialize the variables based on the command line arguments 
			 */
			init(args);
			
			/**
			 * 2: Parse the dictionary.
			 */
			NumberMatcher numberMatcher = new NumberMatcher(getDictionaryParser()
					.parseFile(getDictionaryPath()));
			
			/**
			 * 3: Format numbers in the input. Also keep original numbers to display in the output.
			 */
			Map<String, List<String>> formattedToUnformattedNumbersMap = getPhoneNumberParser().parseFile(getInputFilePath());
			
			/**
			 * 4: Prepare a list of formatted numbers
			 */
			List<String> inputNumbersList = getPhoneNumberParser().getMapKeysAsList(formattedToUnformattedNumbersMap);
			
			/**
			 * 5: Search the dictionary to find the possible matches
			 */
			Map<String, List<String>> numberToWordMap = numberMatcher.findMatchingWordsForListOfNumbers(inputNumbersList);
			
			/**
			 * 6: Write the result
			 */
			getPhoneNumberParser().writeMapToOutput(numberToWordMap, formattedToUnformattedNumbersMap, 
					Constants.OUTPUT_FILE_PATH);
			
			/**
			 * 7: Let user know about the output file location if the input was received in a file.
			 */
			displaySuccessMessage();
			
		} catch(Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw e;
		}

	}
	
	private static void init(String[] args) throws Exception {
		
		dictionaryParser = new DictionaryParser();
		phoneNumberParser = new CommandLineParserWriter();
		dictionaryPath = Constants.DICTIONARY_PATH;
		inputFilePath = "";
		
		switch (args.length) {
			case 0: {
				// No need to initialize anything here. Defaults will be used.
				break;
			}
			case 1: {
				inputFilePath = args[0];
				phoneNumberParser = new PhoneFileParserWriter();
				break;
			}
			case 2: {
				
				if(args[0].equalsIgnoreCase(Constants.COMMAND_LINE_OPTION_DICTIONARY)) {
					dictionaryPath = args[1];
				} else {
					throw new InvalidCommandException(Constants.ERR_INVALID_COMMAND);
				}
				break;
			}
			case 3: {
				
				if(args[0].equalsIgnoreCase(Constants.COMMAND_LINE_OPTION_DICTIONARY)) {

					dictionaryPath = args[1];
					inputFilePath = args[2];
					phoneNumberParser = new PhoneFileParserWriter();

				} else if(args[1].equalsIgnoreCase(Constants.COMMAND_LINE_OPTION_DICTIONARY)) {
					
					inputFilePath = args[0];
					dictionaryPath = args[2];
					phoneNumberParser = new PhoneFileParserWriter();
					
				} else {
					throw new InvalidCommandException(Constants.ERR_INVALID_COMMAND);
				}
				
				break;
			}
			default: {
				
				throw new InvalidCommandException(Constants.ERR_INVALID_COMMAND);
			}
		}
		
	}
	
	private static void congigureLogManager() throws Exception {
		LogManager logManager = LogManager.getLogManager();
		InputStream inputStream = new FileInputStream(new File(Constants.LOGGING_CONFIG_PATH));
		logManager.readConfiguration(inputStream);
		inputStream.close();
	}
	
	private static void displaySuccessMessage() {
		if(getPhoneNumberParser() instanceof PhoneFileParserWriter) {
			System.out.println("The output of this program is stored in " + Constants.OUTPUT_FILE_PATH);
		}
	}

	public static Parser getDictionaryParser() {
		return dictionaryParser;
	}

	public static void setDictionaryParser(Parser dictionaryParser) {
		NumberConverter.dictionaryParser = dictionaryParser;
	}

	public static NumberParserWriter getPhoneNumberParser() {
		return phoneNumberParser;
	}

	public static void setPhoneNumberParser(NumberParserWriter phoneNumberParser) {
		NumberConverter.phoneNumberParser = phoneNumberParser;
	}

	public static String getDictionaryPath() {
		return dictionaryPath;
	}

	public static void setDictionaryPath(String dictionaryPath) {
		NumberConverter.dictionaryPath = dictionaryPath;
	}

	public static String getInputFilePath() {
		return inputFilePath;
	}

	public static void setInputFilePath(String inputFilePath) {
		NumberConverter.inputFilePath = inputFilePath;
	}	
}
