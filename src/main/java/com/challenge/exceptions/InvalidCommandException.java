package com.challenge.exceptions;

public class InvalidCommandException extends Exception {

	private static final long serialVersionUID = 7025523077521893485L;

	public InvalidCommandException(String message) {
		super(message);
	}
}
