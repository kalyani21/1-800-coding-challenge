package com.challenge.exceptions;

public class FileEmptyException extends Exception {

	private static final long serialVersionUID = -8675264449515136926L;

	public FileEmptyException(String message) {
		super(message);
	}
}
