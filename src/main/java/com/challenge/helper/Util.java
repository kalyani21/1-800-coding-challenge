package com.challenge.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.challenge.exceptions.FileEmptyException;

public class Util {

	private static final Logger LOG = Logger.getLogger(Util.class.getName());

	public static String formatOutput(String originalNumber, 
			String formattedNumber, 
			List<String> matchingWords) {
		String unformattedNumber = Constants.DOUBLE_QUOTE + originalNumber + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE;
		String cleanNumber = Constants.DOUBLE_QUOTE + formattedNumber + Constants.DOUBLE_QUOTE + Constants.WHITE_SPACE;
		String words = (matchingWords == null ? 
							Constants.DOUBLE_QUOTE + Constants.DOUBLE_QUOTE  : matchingWords.toString());
		return (unformattedNumber + cleanNumber + words);
	}
	
	
	public static boolean isNullOrEmpty(Object obj) {
		boolean isEmpty = false;
		
		if(obj == null) {
			isEmpty = true;
		} else if(obj instanceof String) {
			if(obj.equals(Constants.EMPTY_STR)) {
				isEmpty = true;
			}
		} else if(obj instanceof List) {
			if(((List) obj).size() == 0) {
				isEmpty = true;
			}
		} else if(obj instanceof Map) {
			if(((Map) obj).size() == 0) {
				isEmpty = true;
			}
		}
		
		return isEmpty;
	}
	
	public static void verifyFile(String filePath) 
			throws IllegalArgumentException, FileNotFoundException, FileEmptyException {
		
		// Verify the file format
		if(!filePath.endsWith(".txt")) {
			IllegalArgumentException e = new IllegalArgumentException(Constants.ERR_INCORRECT_FILE_FORMAT);
			LOG.log(Level.SEVERE, Constants.ERR_INCORRECT_FILE_FORMAT, e);
			throw e;
		}
		
		File file = new File(filePath);
		
		// Verify that file exists
		if(!file.exists()) {
			FileNotFoundException e = new FileNotFoundException(Constants.ERR_FILE_MISSING);
			LOG.log(Level.SEVERE, Constants.ERR_FILE_MISSING, e);
			throw e;
		}
		
		// Verify that the file is not empty
		if(file.length() == 0) {
			FileEmptyException e = new FileEmptyException(Constants.ERR_FILE_EMPTY);
			LOG.log(Level.SEVERE, Constants.ERR_FILE_EMPTY, e);
			throw e;
		}
		
	}
}
