package com.challenge.helper;

import java.util.HashMap;
import java.util.Map;

public class Constants {
	
	public static final String DICTIONARY_PATH = ".//src//main//resources//Dictionary.txt";
	public static final String OUTPUT_FILE_PATH = ".//output//PhoneNumberMapping.txt";
	public static final String LOGGING_CONFIG_PATH = ".//conf//logging.properties";
	
	// Number encoding is as follows.. 2-ABC, 3-DEF, 4-GHI, 5-JKL, 6-MNO, 7-PQRS, 8-TUV, 9-WXYZ
	public static Map<String, String> numberEncoding;
	
	static {
		numberEncoding = new HashMap<String, String>();
		numberEncoding.put("A", "2");
		numberEncoding.put("B", "2");
		numberEncoding.put("C", "2");
		numberEncoding.put("D", "3");
		numberEncoding.put("E", "3");
		numberEncoding.put("F", "3");
		numberEncoding.put("G", "4");
		numberEncoding.put("H", "4");
		numberEncoding.put("I", "4");
		numberEncoding.put("J", "5");
		numberEncoding.put("K", "5");
		numberEncoding.put("L", "5");
		numberEncoding.put("M", "6");
		numberEncoding.put("N", "6");
		numberEncoding.put("O", "6");
		numberEncoding.put("P", "7");
		numberEncoding.put("Q", "7");
		numberEncoding.put("R", "7");
		numberEncoding.put("S", "7");
		numberEncoding.put("T", "8");
		numberEncoding.put("U", "8");
		numberEncoding.put("V", "8");
		numberEncoding.put("W", "9");
		numberEncoding.put("X", "9");
		numberEncoding.put("Y", "9");
		numberEncoding.put("Z", "9");
	}
	
	public static final String WHITE_SPACE = " ";
	public static final String DASH = "-";
	public static final String EMPTY_STR = "";
	public static final String DOUBLE_QUOTE = "\"";
	public static final String NEW_LINE = "\r\n";
	
	public static final String COMMAND_LINE_OPTION_DICTIONARY = "-d";
	
	public static final String OUTPUT_HEADER = "-----------------"
			+ "The output is in the format as specified below"
			+ "-----------------\n\n"
			+ "\"Original_Number_From_Input\" \"Formatted_Number\" \"Comma separated list of matches\"\n\n";
	
	public static final String COMMAND_LINE_MESSAGE = "Please enter the phone numbers below. Enter one number per line."
			+ " Press Ctrl+Z and ENTER when you are done.\n";
	
	public static final String ERR_INVALID_COMMAND = "Invalid Command. Use following syntaxt to execute the command..\n"
												   + "java NumberConverter [<inputFilePath>] [-d <dictionaryPath>]";
	public static final String ERR_INCORRECT_FILE_FORMAT = "Incorrect file format.Only '*.txt' files are allowed.";
	public static final String ERR_FILE_MISSING = "The specified file does not exist.";
	public static final String ERR_FILE_EMPTY = "The specified file is empty.";
}
